<div class="middlePage">
    <div class="page-header">
        <h1 class="logo">FaveList <small>Добро пожаловать!!!!</small></h1>
    </div>

    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title">Пожалуйста авторизируйтесь</h3>
        </div>
        <div class="panel-body">

            <div class="row">

                <div class="col-md-5" >
                    <a href="#" class="btn btn-info"><i class="fa fa-vk fa-2x"></i></i></a><br/>
                </div>

                <div class="col-md-7" style="border-left:1px solid #ccc;height:160px">
                    <form class="form-horizontal" method="post" action="/authorization/">
                        <fieldset>
                            <input id="textinput" name="userlogin" type="text" placeholder="Введите Логин" class="form-control input-md">
                            <div class="spacing"><input type="checkbox" name="remember" id="checkboxes-0" value="1"><small> Запомнить меня</small></div>
                            <input id="textinput" name="userPass" type="text" placeholder="Введите пароль" class="form-control input-md">
                            <div class="spacing"><a href="#"><small> Забыли пароль?</small></a><br/></div>
                            <button id="loginButton" name="loginButton" class="btn btn-info btn-sm pull-right">Войти</button>
                            <a href="/regestration/" class="btn btn-danger btn-sm pull-left">Регистрация</a>
                        </fieldset>
                    </form>
                </div>

            </div>

        </div>
    </div>

    <p><a href="#">О нас</a> </p>

</div>
<script type="text/javascript" src="/lib/ajax/auth.js"></script>