<? $users = $this->ion_auth->users()->result(); ?>
<div id="page-wrapper">

    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Управление пользователями
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="/adminarea/">Админ Панель</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-desktop"></i> Управления пользователями
                    </li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Основные действия</h3>
                    </div>
                    <div class="panel-body">
                        <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#createUser">
                            Создать Пользователя
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Пользватели</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Логин</th>
                                            <th>E-mail</th>
                                            <th>Активный</th>
                                            <th>Группа</th>
                                            <th>Действия</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <? foreach($users as $user):
                                            $groups = $this->ion_auth->get_users_groups($user->id)->result();
                                        ?>
                                        <tr>
                                            <td><?=$user->username; ?></td>
                                            <td><?=$user->email; ?></td>
                                            <? if($user->active == 1): ?>
                                                <td><i class="fa fa-check"></i></td>
                                            <? else: ?>
                                                <td><i class="fa fa-close"></i></td>
                                            <? endif; foreach($groups as $group):?>
                                            <td><?=$group->name; ?></td>
                                            <? endforeach; ?>
                                            <td>
                                                <a href="/adminarea/users/edit/<?=$user->id; ?>" class="btn btn-xs btn-success">Редактировать</a>
                                                <a href="/adminarea/users/delete/<?=$user->id; ?>" class="btn btn-xs btn-danger">Удалить</a>
                                            </td>
                                        </tr>
                                        <? endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                    </div>
                </div>
        </div>
    </div>
            <!-- Окно для создания пользователя -->
<div class="modal fade" id="createUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Создать пользователя</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="/adminarea/users/create/">
                    <div class="form-group">
                        <label>Имя пользователя</label>
                        <input class="form-control" name="username">
                    </div>
                    <div class="form-group">
                        <label>Фамилия пользователя</label>
                        <input class="form-control" name="userfam">
                    </div>
                    <div class="form-group">
                        <label>Логин пользователя</label>
                        <input class="form-control" name="userlogin">
                    </div>
                    <div class="form-group">
                        <label>E-mail пользователя</label>
                        <input class="form-control" name="useremail">
                    </div>
                    <div class="form-group">
                        <label>Пароль пользователя</label>
                        <input class="form-control" name="userpass">
                    </div>
                    <div class="form-group">
                        <label>Активирован?</label>
                        <select class="form-control" name="useract">
                            <option value="1">Да</option>
                            <option value="0">Нет</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="Создать пользователя">
                </div>
                </form>
        </div>
    </div>
</div>