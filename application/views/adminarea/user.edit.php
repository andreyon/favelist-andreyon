<? $user = $this->ion_auth->user($id)->row(); ?>
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Редактирование пользователя - <?=$user->username; ?>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="/adminarea/">Админ Панель</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-desktop"></i> Управления пользователями
                    </li>
                </ol>
            </div>
        </div>
        <form role="form">
            <div class="form-group" method="post">
                <label>Имя пользователя</label>
                <input class="form-control" name="username" value="<?=$user->first_name; ?>">
            </div>
            <div class="form-group">
                <label>Фамилия пользователя</label>
                <input class="form-control" name="userfamily" value="<?=$user->last_name; ?>">
            </div>
            <div class="form-group">
                <label>Логин пользователя</label>
                <input class="form-control" name="userlogin" value="<?=$user->username; ?>">
            </div>
            <div class="form-group">
                <label>E-mail пользователя</label>
                <input class="form-control" name="useremail" value="<?=$user->email; ?>">
            </div>
            <div class="form-group">
                <label>Активирован?</label>
                <select class="form-control" name="useract">
                    <option value="1">Да</option>
                    <option value="0">Нет</option>
                </select>
            </div>
            <input type="submit" class="btn btn-primary" value="Применить">
            <a href="/adminarea/users/" class="btn btn-success">Отмена</a>
        </form>
    </div>
</div>