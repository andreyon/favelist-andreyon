<?php

class Adminarea extends CI_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('message');
        if(!$this->ion_auth->is_admin())
        {
            redirect('/');
        }
    }

    public function index()
    {
        if($this->ion_auth->is_admin())
        {
            $this->load->view('adminarea/header');
            $this->load->view('adminarea/index');
            $this->load->view('adminarea/footer');
        }
    }

    public function users($action = null, $id = null)
    {
        $this->load->view('adminarea/header');
        if($action == 'delete')
        {
            $this->ion_auth->delete_user($id);
            //success_message('Пользователь успешно удалён.');
        }
        if($action == 'create')
        {
            $userName = $this->input->post('username');
            $userfam = $this->input->post('userfam');
            $login = $this->input->post('userlogin');
            $email = $this->input->post('useremail');
            $pass = $this->input->post('userpass');
            $act = $this->input->post('useract');

            $group = array('2');

            $data = array('first_name' => $userName, 'last_name' => $userfam, 'active' => $act);

            $this->ion_auth->register($login, $pass, $email, $data, $group);
        }else if($action == 'edit')
        {
            if($this->input->get())
            {
                $username = $this->input->get('username');
                $userfamily = $this->input->get('userfamily');
                $login = $this->input->get('userlogin');
                $act = $this->input->get('useract');
                $email = $this->input->get('useremail');

                $datas = array('first_name' => $username, 'last_name' => $userfamily, 'username' => $login,
                    'email' => $email, 'active' => $act);

                if($this->ion_auth->update($id, $datas))
                {
                    redirect('/adminarea/users/', 'refresh');
                }
            }

            $this->data['id'] = $id;
            $this->load->view('adminarea/user.edit.php', $this->data);
        }
        if($action == null)
            $this->load->view('adminarea/users');

        $this->load->view('adminarea/footer');
    }

    public function system()
    {
        $this->load->view('adminarea/header');
        $this->load->view('adminarea/footer');
    }
}