<?

class Ajax extends CI_Controller
{
    public function auth()
    {
        $userName = $this->input->post('userlogin', true);
        $userPass = $this->input->post('userPass', true);
        $remember = $this->input->post('remember');

        if($this->ion_auth->login($userName, $userPass, $remember) == 1)
        {
            redirect('/user/info/', 'refresh');
        }
    }
}