<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MX_Controller
{
    protected $data = array();

    public function __construct()
    {
        parent::__construct();
    }

    public function info()
    {
        $this->load->view('origin/header.php', $this->data);
        $this->load->view('origin/user.php', $this->data);
        $this->load->view('origin/footer.php', $this->data);
    }

    public function logout()
    {
        $this->ion_auth->logout();

        redirect('/', 'refresh');
    }


}