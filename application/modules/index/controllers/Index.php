<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends MX_Controller
{
    protected $data = array();

    public function __construct(){
        parent::__construct();
        $this->load->helper('message');
    }
    public function index()
    {
        $this->load->view('origin/header.php', $this->data);
        if(!$this->ion_auth->logged_in()) {
            $this->load->view('origin/index.php', $this->data);
        }else{
            $this->load->view('origin/user.php', $this->data);
        }
        $this->load->view('origin/footer.php', $this->data);
    }
    public function authorization()
    {
        $userName = $this->input->post('userlogin', true);
        $userPass = $this->input->post('userPass', true);
        $remember = $this->input->post('remember');

        if($this->ion_auth->login($userName, $userPass, $remember) == 1)
        {
            redirect('/user/info/', 'refresh');
        }
    }

    public function regestration()
    {
        $this->data['success_message'] = null;
        if($this->input->post() != NULL)
        {
            $firstName = $this->input->post('first_name', true);
            $lastName = $this->input->post('last_name', true);
            $login = $this->input->post('display_name', true);
            $email = $this->input->post('email');
            $pass = $this->input->post('password');
            $confirmPass = $this->input->post('password_confirmation');

            $additional_data = array('first_name' => $firstName, 'last_name' => $lastName);

            $group = array('2');
            if($pass == $confirmPass) {
                if ($this->ion_auth->register($login, $pass, $email, $additional_data, $group)) {
                    $this->data['success_message'] = true;
                }
            }else{
                $this->data['success_message'] = false;
            }
        }
        $this->load->view('origin/header.php', $this->data);
        $this->load->view('origin/reg.php', $this->data);
        $this->load->view('origin/footer.php', $this->data);
    }
}