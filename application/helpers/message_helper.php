<?php

function success_message($msg)
{
    echo '<p class="bg-success">'.$msg.'</p>';
}

function error_message($msg)
{
    echo '<p class="bg-danger">'.$msg.'</p>';
}